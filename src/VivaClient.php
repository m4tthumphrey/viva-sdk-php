<?php

namespace TravelConnection\Viva;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LogLevel;

use SimpleXMLElement;
use XMLWriter;

class VivaClient implements ClientInterface
{
    use LoggerAwareTrait;

    const NAMESPACE    = 'http://omniticket.network/ovw7';
    const PAYMENT_CODE = 'SALDO';

    private array $config;
    private Client $client;

    private ?string $sessionId = null;

    public function __construct(array $config)
    {
        $this->config = $config;

        $this->createClient();
    }

    private function createClient()
    {
        $this->client = new Client([
            'base_uri' => $this->config['base_uri'],
            'headers'  => [
                'Connection'   => 'Keep-Alive',
                'User-Agent'   => 'TW',
                'Content-Type' => 'text/xml; charset=utf-8'
            ]
        ]);
    }

    private function post(string $service, string $method, callable $writer = null)
    {
        $namespace = sprintf('urn:%sIntf-I%s', $service, $service);
        $url       = '/soap/' . $service;

        $xml = new XMLWriter();
        $xml->openMemory();
        $xml->startDocument('1.0', 'utf-8');
        $xml->startElement('SOAP-ENV:Envelope');

        $xml->writeAttributeNs('xmlns', 'SOAP-ENV', null, 'http://schemas.xmlsoap.org/soap/envelope/');
        $xml->writeAttributeNs('xmlns', 'xsi', null, 'http://www.w3.org/2001/XMLSchema-instance');
        $xml->writeAttributeNs('xmlns', 'ns1', null, $namespace);
        $xml->writeAttributeNs('xmlns', 'ovw7', null, $namespace);
        $xml->writeAttributeNs('xmlns', 'urn', null, $namespace);

        $xml->startElement('SOAP-ENV:Body');
        $xml->startElement('ns1:' . $method);

        if (is_callable($writer)) {
            call_user_func($writer, $xml);
        }

        $xml->endElement();
        $xml->endElement();
        $xml->endElement();
        $xml->endDocument();

        $body = $xml->outputMemory();

        $this->log($body);

        try {
            $response = $this->client->post($url, [
                'headers' => [
                    'SOAPAction'     => $namespace . '#' . $method,
                    'Content-Length' => strlen($body)
                ],
                'body'    => $body,
                'query'   => $this->sessionId ? 'OvwSessionId=' . $this->sessionId : ''
            ]);
        } catch (ServerException $e) {
            $this->log($e->getResponse()->getBody()->getContents(), LogLevel::ERROR);

            throw new VivaException('The server encountered an unexpected error');
        }

        $xml = $response->getBody()->getContents();

        if (false !== strpos($xml, 'SOAP-ENV:Fault')) {
            $xml  = new SimpleXMLElement($xml);
            $data = $xml
                ->children('SOAP-ENV', true)
                ->Body;

            $fault = $data->Fault->children();

            throw new VivaException($fault->faultstring);
        }

        $xml  = new SimpleXMLElement($xml);
        $data = $xml
            ->children('SOAP-ENV', true)
            ->Body
            ->children($namespace)
            ->{$method . 'Response'}
            ->children(self::NAMESPACE)
            ->return;

        if (isset($data->ERROR) && 200 !== (int) $data->ERROR->CODE) {
            throw new VivaException($data->ERROR->TEXT, (int) $data->ERROR->CODE);
        }

        return json_decode(json_encode($data), true);
    }

    public function login(): string
    {
        $response = $this->post('WsAPIUser', 'UserLogin', function (XMLWriter $xml) {
            $xml->writeElement('AWorkstationAK', $this->config['workstation']);
            $xml->writeElement('AUserName', $this->config['username']);
            $xml->writeElement('APassword', $this->config['password']);
        });

        $this->authenticate($response['SESSIONID']);

        return $this->sessionId;
    }

    public function authenticate(string $sessionId)
    {
        $this->sessionId = $sessionId;
    }

    public function getEvents(): array
    {
        $response = $this->post('WsAPIEvent', 'FindAllEvents');

        return $response['EVENTLIST']['EVENT'];
    }

    public function getPerformances(string $ak, string $from, string $to, array $options = []): array
    {
        // @TODO handle SELLABLE
        // @TODO handle RANGEPRICE
        // @TODO handle ATTRIBUTEAK
        // @TODO handle CARTPERFORMANCELIST

        $response = $this->post('WsAPIPerformance', 'GetCalendarAvailability', function (XMLWriter $xml) use ($ak, $from, $to) {
            $xml->startElement('ovw7:GETCALENDARAVAILABILITYREQ');
            $xml->writeElement('EVENTAK', $ak);
            $xml->writeElement('FROMDATE', $from);
            $xml->writeElement('TODATE', $to);
            $xml->endElement();
        });

        return $response['DAYLIST']['DAY'];
    }

    public function getProductsForPerformance(string $ak): array
    {
        $response = $this->post('WsAPIPerformance', 'FindAllPerformanceProductByAk', function (XMLWriter $xml) use ($ak) {
            $xml->startElement('ovw7:FINDALLPERFORMANCEPRODUCTBYAKREQ');
            $xml->writeElement('PERFORMANCEAK', $ak);
        });

        return $response['PRODUCTLIST']['PRODUCT'];
    }

    public function getProductInfo(string $ak)
    {
        $response = $this->post('WsAPIProduct', 'ReadProductByAK', function (XMLWriter $xml) use ($ak) {
            $xml->writeElement('AProductAK', $ak);
        });

        return $response['PRODUCT'];
    }

    public function checkout(int $accountAk, string $externalCode, array $items, string $languageCode = ClientInterface::DEFAULT_LANGUAGE_CODE, array $flags = [])
    {
        $response = $this->post('WsAPIOrder', 'CheckOut', function (XMLWriter $xml) use ($externalCode, $accountAk, $languageCode, $items) {
            $xml->startElement('CheckOutReq');
            $xml->startElement('SHOPCART');
            $xml->writeAttribute('xsi:type', 'ovw7:SHOPCART');

            $xml->startElement('ITEMLIST');

            foreach ($items as $item) {
                $xml->startElement('ITEM');
                $xml->writeElement('AK', $item['item']);
                $xml->writeElement('QTY', $item['quantity']);
                $xml->startElement('PERFORMANCELIST');
                $xml->startElement('PERFORMANCE');
                $xml->writeElement('AK', $item['performance']);
                $xml->endElement();
                $xml->endElement();
                $xml->endElement();
            }

            $xml->endElement();

            $xml->startElement('FLAG');
            $xml->writeElement('APPROVED', true);
            $xml->writeElement('PAID', true);
            $xml->writeElement('ENCODED', true);
            $xml->writeElement('VALIDATED', true);
            $xml->writeElement('COMPLETED', true);
            $xml->endElement();

            $xml->startElement('RESERVATION');
            $xml->writeElement('EXTERNALCODE', $externalCode);

            $xml->startElement('BILLINGACCOUNT');
            $xml->writeElement('AK', $accountAk);
            $xml->endElement();

            $xml->startElement('LOGGEDACCOUNT');
            $xml->writeElement('AK', $accountAk);
            $xml->endElement();

            $xml->startElement('RESERVATIONOWNER');
            $xml->writeElement('AK', $accountAk);
            $xml->endElement();

            $xml->writeElement('LANGUAGEAK', $languageCode);

            $xml->endElement();
            $xml->endElement();
        });

        unset($response['ERROR']);

        return $response;
    }

    public function closeOrder(string $ak, string $amount)
    {
        $response = $this->post('WsAPIOrder', 'CloseOrder', function (XMLWriter $xml) use ($ak, $amount) {
            $xml->startElement('CLOSEORDERREQ');
            $xml->writeElement('AK', $ak);
            $xml->startElement('PAYMENTINFOLIST');
            $xml->startElement('PAYMENTINFO');
            $xml->writeElement('CODE', self::PAYMENT_CODE);
            $xml->writeElement('AMOUNT', $amount);
            $xml->endElement();
            $xml->endElement();
            $xml->endElement();
        });

        unset($response['ERROR']);

        return $response;
    }

    public function getTicketPdf(string $ak)
    {
        $response = $this->post('WsAPITicket', 'PrintPdfTicket', function(XMLWriter $xml) use ($ak) {
            $xml->writeElement('ASaleAk', $ak);
        });

        return hex2bin($response['PDF']);
    }

    protected function log(string $message, string $level = LogLevel::DEBUG, array $context = [])
    {
        if ($this->logger) {
            $this->logger->log($level, $message, $context);
        }
    }
}