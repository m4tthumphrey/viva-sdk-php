<?php

namespace TravelConnection\Viva;

interface ClientInterface
{
    const DEFAULT_LANGUAGE_CODE = 'en-GB';

    public function login(): string;

    public function authenticate(string $sessionId);

    public function getEvents(): array;

    public function getPerformances(string $ak, string $from, string $to, array $options = []): array;

    public function getProductsForPerformance(string $ak): array;

    public function getProductInfo(string $ak);

    public function checkout(int $accountAk, string $externalCode, array $items, string $languageCode = ClientInterface::DEFAULT_LANGUAGE_CODE, array $flags = []);

    public function closeOrder(string $ak, string $amount);
}