# viva-sdk-php

Client to interact with the Viva ticket SOAP APIs.

Note: Access to the API is restricted by IP whitelisting.

## Config

```json
{
    "base_uri": "http://preprod-services-fcb.vivaticket.com",
    "workstation": "",
    "username": "",
    "password": ""
}
```

## Basic use

The `post` method allows to easily make an XML request to the API. The first argument references the specific service or API. The second is the specific API method name you wish to call.

The final parameter accepts a `callable` that can used to construct your XML request using the standard PHP library `XMLWriter`.

```php
$client = new VivaClient($config);
$data = $client->post('WsAPIEvent', 'FunctionName', function(XMLWriter $xml) {
    $xml->writeElement('ElementName', 'Value');
});
```

This will return a JSON associative array response.

## Authentication

Ensure your config contains the correct credentials.

### Logging in

To obtain a new session ID (token).

```php
$sessionId = $client->login();
```

On successful authentication, the new session ID will be returned as well as being set on the client with no need to reauthenticate until the session expires.

### Use an existing session ID

If you already have a valid session ID (stored in config or elsewhere) you can skip logging in and pass the session to the client directly.

```php
$client->authenticate($sessionId);
```

## Error handling

Any errors thrown when interacting with the client will throw a new instance of `VivaException`.

```php
try {
    $data = $client->post('WsAPIEvent', 'FunctionName');
} catch (VivaException $e) {
    echo $e->getMessage() . ' (' . $e->getCode() . ')';
}
```